# Nextcloud Setup

This project contains the current setup of my server.
It is fully build on docker, therefore you should be able to run this on any decently new system.
I've based my build on [this](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/with-nginx-proxy/mariadb/fpm/docker-compose.yml) example configuration from nextcloud, but slightly altered.


## What is included

This setup includes:

* nginx proxy
* letsencrypt-companion
* mariadb database
* redis database
* nextcloud nginx webserver
* nextcloud app

Below I will outline what each part does.


### nginx Proxy

This nginx container (based on the [nginx-proxy](https://hub.docker.com/r/nginxproxy/nginx-proxy) image) is the only container exposed to the outside world.
It can automatically detect other containers which are supposed to run on subdomains and sets up a proxy to these subdomains.
This means, whenever you start a container which defines the `VIRTUAL_HOST` environment variable, you can reach that container under this subdomain.
This is super helpful, if you want to quickly run a small project in a container exposed to the internet.
Note that all containers for which this should work, must be part of the same network as that container (in my setup `nginx-proxy`).

I do not use the image directly, but a (very) slightly altered version from Nextcloud.
Basically, there version changes the uploadsize allowed, it can be found [here](https://github.com/nextcloud/docker/tree/master/.examples/docker-compose/with-nginx-proxy/mariadb/fpm/proxy).

### Letsencrypt Companion

This is the [nginxproxy/acme-companion](https://hub.docker.com/r/nginxproxy/acme-companion) image.
It can easily handle ssl certificates for all subdomains, which the nginx proxy sets up.
For this to work you must define the `LETSENCRYPT_HOST` environment variable to the same as the subdomain.
Therefore, your containers can have super easy ssl!

This container needs to define the environment variables for the proxy and letsencrypt setup. On my setup this is:

    environment:
      - VIRTUAL_HOST=cube.szlagowski.xyz
      - LETSENCRYPT_HOST=cube.szlagowski.xyz
      - LETSENCRYPT_EMAIL=lion-sz@gmx.de

Defining the email means that I get notifications if something is wrong and my certificates do not get renewed.

### MariaDB Database

This is the database container I use, an unmodified MariaDB image.
Note, that I mount a host folder `/nextcloud_db` as the folder where the database stores it's data.
If you want, you can back this up, or even move it to an unnamed volume.

This container mounts the local path `/nextcloud` and places the files there.
On my machine this then links to the path, where I mount my ZFS share and which I can back up.

### Redis Database

This I believe is optional, Nextcloud should work without a Redis server.
But since the example used it, I also got it.
I don't know much about Redis, though.


### Nextcloud Webserver

This is the webserver, which serves the Nextcloud files.
It is build from the web directory and base on the nginx/alpine image.
This is directly copied from the nextcloud examples on how to run docker.
Specifically, I used [this](https://github.com/nextcloud/docker/tree/master/.examples/docker-compose/with-nginx-proxy/mariadb/fpm/web) example from Nextcloud.


### Nextcloud App

This is the heart of the setup.
I've used the [full fpm](https://github.com/nextcloud/docker/tree/master/.examples/dockerfiles/full/fpm-alpine) build, which contains basically everything (including the cron job) in one container.
It installs a whole bunch of stuff and is therefore a pretty large container.
Basically, the only not installed package is libreoffice, but I might enable this once I need it.
Without it, online editing might not work, but I've not tested this.

I've set the `OVERWRITEPROTOCOL` environment variable to https, but I'm not sure if I still need it.
I'm to lazy to test this right now and this option seems to be very save (https sounds good, right?).

## Volumes

Most volumes which I have, are named, meaning they persist between rebuilding the setup (if I understand this correctly).
Only the database and nextcloud files are directly linked to a folder on the host drive.


## Networks

For the proxy to work I need to set up a network: `nginx-proxy`.
Both the letsencrypt companion, the proxy and the webserver container need to be part of the network.
But once I had timeouts, because the webserver could not reach the app and adding the app to the network fixed this.
Therefore, I believe the app needs to be part of the network as well.

## Database Config

I need to set up database passwords and so on.
For this, I've got the `db.env` file, you can change the passwords here.